﻿using System;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace SWGVOHTREFix
{
    public partial class TREFix : Form
    {
        private const byte MAX_PATCH = 6;
        private static readonly string PATCH_LIST = "https://geniusgaming.net/cfgs/main.cfg";

        public TREFix()
        {
            InitializeComponent();
        }

        private bool TreIsBad(string stream, int i)
        {
            if (!File.Exists(Application.StartupPath + @"\voh_patch_0" + i + ".tre"))
                return true;

            using (StringReader reader = new StringReader(stream))
            {
                string line = reader.ReadLine();

                while (line != null)
                {
                    if (line.Contains("voh_patch_0" + i))
                        return int.Parse(line.Replace("voh_patch_0" + i + ".tre\t", string.Empty)) != new FileInfo(Application.StartupPath + @"\voh_patch_0" + i + ".tre").Length;
                    line = reader.ReadLine();
                }
            }

            return true;
        }

        private void ScanButton_Click(object sender, EventArgs e)
        {
            OutputListbox.Items.Clear();

            try
            {
                using (WebClient client = new WebClient())
                {
                    string patchList = client.DownloadString(PATCH_LIST);

                    for (int i = 0; i < MAX_PATCH + 1; i++)
                        OutputListbox.Items.Add("Checking voh_patch_0" + i + ".tre...." + (TreIsBad(patchList, i) ? "NEEDS PATCHED" : "UP TO DATE"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
