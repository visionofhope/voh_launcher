﻿namespace UserCfgManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.CreateButton = new System.Windows.Forms.Button();
            this.GodModeCommandsCheckBox = new System.Windows.Forms.CheckBox();
            this.DebugClipboardExamineCheckBox = new System.Windows.Forms.CheckBox();
            this.AllowTargetAnythingCheckBox = new System.Windows.Forms.CheckBox();
            this.DrawNetworkIdsCheckBox = new System.Windows.Forms.CheckBox();
            this.DebugExamineCheckBox = new System.Windows.Forms.CheckBox();
            this.ExtendMaxZoomCheckBox = new System.Windows.Forms.CheckBox();
            this.TitleLabel = new System.Windows.Forms.Label();
            this.ZoomTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // CreateButton
            // 
            this.CreateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateButton.Location = new System.Drawing.Point(105, 226);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(75, 23);
            this.CreateButton.TabIndex = 0;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // GodModeCommandsCheckBox
            // 
            this.GodModeCommandsCheckBox.AutoSize = true;
            this.GodModeCommandsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GodModeCommandsCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.GodModeCommandsCheckBox.Location = new System.Drawing.Point(27, 62);
            this.GodModeCommandsCheckBox.Name = "GodModeCommandsCheckBox";
            this.GodModeCommandsCheckBox.Size = new System.Drawing.Size(209, 20);
            this.GodModeCommandsCheckBox.TabIndex = 1;
            this.GodModeCommandsCheckBox.Text = "Enable God Mode Commands";
            this.GodModeCommandsCheckBox.UseVisualStyleBackColor = true;
            // 
            // DebugClipboardExamineCheckBox
            // 
            this.DebugClipboardExamineCheckBox.AutoSize = true;
            this.DebugClipboardExamineCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DebugClipboardExamineCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.DebugClipboardExamineCheckBox.Location = new System.Drawing.Point(27, 88);
            this.DebugClipboardExamineCheckBox.Name = "DebugClipboardExamineCheckBox";
            this.DebugClipboardExamineCheckBox.Size = new System.Drawing.Size(231, 20);
            this.DebugClipboardExamineCheckBox.TabIndex = 2;
            this.DebugClipboardExamineCheckBox.Text = "Enable Debug Clipboard Examine";
            this.DebugClipboardExamineCheckBox.UseVisualStyleBackColor = true;
            // 
            // AllowTargetAnythingCheckBox
            // 
            this.AllowTargetAnythingCheckBox.AutoSize = true;
            this.AllowTargetAnythingCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllowTargetAnythingCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.AllowTargetAnythingCheckBox.Location = new System.Drawing.Point(27, 114);
            this.AllowTargetAnythingCheckBox.Name = "AllowTargetAnythingCheckBox";
            this.AllowTargetAnythingCheckBox.Size = new System.Drawing.Size(202, 20);
            this.AllowTargetAnythingCheckBox.TabIndex = 3;
            this.AllowTargetAnythingCheckBox.Text = "Enable Allow Target Anything";
            this.AllowTargetAnythingCheckBox.UseVisualStyleBackColor = true;
            // 
            // DrawNetworkIdsCheckBox
            // 
            this.DrawNetworkIdsCheckBox.AutoSize = true;
            this.DrawNetworkIdsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DrawNetworkIdsCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.DrawNetworkIdsCheckBox.Location = new System.Drawing.Point(27, 140);
            this.DrawNetworkIdsCheckBox.Name = "DrawNetworkIdsCheckBox";
            this.DrawNetworkIdsCheckBox.Size = new System.Drawing.Size(179, 20);
            this.DrawNetworkIdsCheckBox.TabIndex = 4;
            this.DrawNetworkIdsCheckBox.Text = "Enable Draw Network IDs";
            this.DrawNetworkIdsCheckBox.UseVisualStyleBackColor = true;
            // 
            // DebugExamineCheckBox
            // 
            this.DebugExamineCheckBox.AutoSize = true;
            this.DebugExamineCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DebugExamineCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.DebugExamineCheckBox.Location = new System.Drawing.Point(27, 166);
            this.DebugExamineCheckBox.Name = "DebugExamineCheckBox";
            this.DebugExamineCheckBox.Size = new System.Drawing.Size(169, 20);
            this.DebugExamineCheckBox.TabIndex = 5;
            this.DebugExamineCheckBox.Text = "Enable Debug Examine";
            this.DebugExamineCheckBox.UseVisualStyleBackColor = true;
            // 
            // ExtendMaxZoomCheckBox
            // 
            this.ExtendMaxZoomCheckBox.AutoSize = true;
            this.ExtendMaxZoomCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExtendMaxZoomCheckBox.ForeColor = System.Drawing.SystemColors.Control;
            this.ExtendMaxZoomCheckBox.Location = new System.Drawing.Point(27, 192);
            this.ExtendMaxZoomCheckBox.Name = "ExtendMaxZoomCheckBox";
            this.ExtendMaxZoomCheckBox.Size = new System.Drawing.Size(166, 20);
            this.ExtendMaxZoomCheckBox.TabIndex = 6;
            this.ExtendMaxZoomCheckBox.Text = "Extend Maximum Zoom";
            this.ExtendMaxZoomCheckBox.UseVisualStyleBackColor = true;
            this.ExtendMaxZoomCheckBox.CheckedChanged += new System.EventHandler(this.ExtendMaxZoomCheckBox_CheckedChanged);
            // 
            // TitleLabel
            // 
            this.TitleLabel.AutoSize = true;
            this.TitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TitleLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.TitleLabel.Location = new System.Drawing.Point(73, 9);
            this.TitleLabel.Name = "TitleLabel";
            this.TitleLabel.Size = new System.Drawing.Size(156, 40);
            this.TitleLabel.TabIndex = 7;
            this.TitleLabel.Text = "SWG Vision of Hope\r\nUser Config Editor";
            this.TitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ZoomTextBox
            // 
            this.ZoomTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ZoomTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ZoomTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoomTextBox.ForeColor = System.Drawing.SystemColors.Control;
            this.ZoomTextBox.Location = new System.Drawing.Point(199, 193);
            this.ZoomTextBox.MaxLength = 4;
            this.ZoomTextBox.Name = "ZoomTextBox";
            this.ZoomTextBox.Size = new System.Drawing.Size(45, 15);
            this.ZoomTextBox.TabIndex = 8;
            this.ZoomTextBox.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ZoomTextBox);
            this.Controls.Add(this.TitleLabel);
            this.Controls.Add(this.ExtendMaxZoomCheckBox);
            this.Controls.Add(this.DebugExamineCheckBox);
            this.Controls.Add(this.DrawNetworkIdsCheckBox);
            this.Controls.Add(this.AllowTargetAnythingCheckBox);
            this.Controls.Add(this.DebugClipboardExamineCheckBox);
            this.Controls.Add(this.GodModeCommandsCheckBox);
            this.Controls.Add(this.CreateButton);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Config Editor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.CheckBox GodModeCommandsCheckBox;
        private System.Windows.Forms.CheckBox DebugClipboardExamineCheckBox;
        private System.Windows.Forms.CheckBox AllowTargetAnythingCheckBox;
        private System.Windows.Forms.CheckBox DrawNetworkIdsCheckBox;
        private System.Windows.Forms.CheckBox DebugExamineCheckBox;
        private System.Windows.Forms.CheckBox ExtendMaxZoomCheckBox;
        private System.Windows.Forms.Label TitleLabel;
        private System.Windows.Forms.TextBox ZoomTextBox;
    }
}

