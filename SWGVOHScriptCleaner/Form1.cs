﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SWGVOHScriptCleaner
{
    public partial class MainForm : Form
    {
        List<string> imports = new List<string>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void AttachButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialog = new OpenFileDialog())
            {
                dialog.Filter = "Java Files (*.java)|*.java";
                
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(dialog.FileName))
                    {
                        FileLabel.Text = "Script Loaded: " + new FileInfo(dialog.FileName).Name;
                        DirectoryTextBox.Text = dialog.FileName;
                    }
                }
            }
        }

        private void CleanupButton_Click(object sender, EventArgs e)
        {
            if (File.Exists(DirectoryTextBox.Text))
            {
                imports.Clear();

                using (StringReader reader = new StringReader(DirectoryTextBox.Text))
                {
                    string line = reader.ReadLine();

                    bool done = false;

                    while (line != null)
                    {
                        MessageBox.Show("Line: " + line);
                        if (done)
                        {
                            string test = lineContainsImport(line);
                            if (test != null)
                                OutputTextBox.Text += "\n" + test;
                        }
                        else if (line.StartsWith("import "))
                        {
                            string[] lineSplit = line.Split('.');
                            imports.Add(lineSplit[lineSplit.Length - 1]);
                        }
                        else if (line.StartsWith("public class "))
                        {
                            done = true;
                        }
                        line = reader.ReadLine();
                    }
                }
            }
            else
            {
                MessageBox.Show("File doesn't exist!");
            }
        }

        private string lineContainsImport(string line)
        {
            for (int i = 0; i < imports.Count; i++)
            {
                if (line.Equals(imports[i]))
                    return imports[i];
            }
            return null;
        }
    }
}
