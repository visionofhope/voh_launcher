﻿using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;

namespace LogViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OutputTextbox.Text = GetLog();
            LogOptionComboBox.SelectedIndex = 0;
        }

        private string GetLog()
        {
            string[] log = new WebClient().DownloadString("http://144.217.55.86/secretl0g$/logz.php").Split('\n');
            string result = string.Empty;

            string[] temp = null;
            for (int i = 0; i < log.Length - 1; i++)
            {

                temp = log[i].Split(':');
                result += "[" + i + "] " + temp[0].Substring(0, 4) + "-" + GetMonth(temp[0].Substring(4, 2)) + "-" + temp[0].Substring(6, 2) + " (" + temp[0].Substring(8, 2) + ":" + temp[0].Substring(10, 2) + ":" + temp[0].Substring(12, 2) + ") : " + temp[1] + "\n";
            }

            return result;
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            OutputTextbox.Text = GetLog();
        }

        private string GetMonth(string Month)
        {
            switch (Month)
            {
                case "01":
                    return "Jan";
                case "02":
                    return "Feb";
                case "03":
                    return "Mar";
                case "04":
                    return "Apr";
                case "05":
                    return "May";
                case "06":
                    return "Jun";
                case "07":
                    return "Jul";
                case "08":
                    return "Aug";
                case "09":
                    return "Sep";
                case "10":
                    return "Oct";
                case "11":
                    return "Nov";
                default:
                    return "Dec";
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            SettingsPanel.Location = new Point(Width - SettingsPanel.Width - 25, SettingsPanel.Location.Y);
            OutputTextbox.Width = SettingsPanel.Location.X - 25;
            OutputTextbox.Height = ClientSize.Height - 20 - SettingsPanel.Location.Y;
            SettingsPanel.Height = OutputTextbox.Height;
        }
    }
}
