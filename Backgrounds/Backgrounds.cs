﻿using System.Drawing;

namespace Backgrounds
{
    public class Backgrounds
    {
        private const byte MAX_INDEX = 5;

        public static Image GetImage(int index)
        {
            return (Image)Properties.Resources.ResourceManager.GetObject("bg" + index);
        }

        public static Image GetLoginBgImage()
        {
            return Properties.Resources.bg_login;
        }

        public static Image GetLauncherBgImage()
        {
            return Properties.Resources.bg_launcher;
        }

        public static byte GetNumberOfImages()
        {
            return MAX_INDEX;
        }
    }
}
