﻿namespace SWGVOHLauncher.forms
{
    partial class AccountDropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MyAccountLabel = new System.Windows.Forms.Label();
            this.SettingsLabel = new System.Windows.Forms.Label();
            this.LogoutLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // MyAccountLabel
            // 
            this.MyAccountLabel.AutoSize = true;
            this.MyAccountLabel.BackColor = System.Drawing.Color.Transparent;
            this.MyAccountLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MyAccountLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyAccountLabel.Location = new System.Drawing.Point(2, 10);
            this.MyAccountLabel.Name = "MyAccountLabel";
            this.MyAccountLabel.Size = new System.Drawing.Size(86, 18);
            this.MyAccountLabel.TabIndex = 0;
            this.MyAccountLabel.Text = "My Account";
            this.MyAccountLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MyAccountLabel_MouseClick);
            this.MyAccountLabel.MouseEnter += new System.EventHandler(this.MyAccountLabel_MouseEnter);
            this.MyAccountLabel.MouseLeave += new System.EventHandler(this.MyAccountLabel_MouseLeave);
            // 
            // SettingsLabel
            // 
            this.SettingsLabel.AutoSize = true;
            this.SettingsLabel.BackColor = System.Drawing.Color.Transparent;
            this.SettingsLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SettingsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SettingsLabel.Location = new System.Drawing.Point(2, 35);
            this.SettingsLabel.Name = "SettingsLabel";
            this.SettingsLabel.Size = new System.Drawing.Size(61, 18);
            this.SettingsLabel.TabIndex = 1;
            this.SettingsLabel.Text = "Settings";
            this.SettingsLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.SettingsLabel_MouseClick);
            this.SettingsLabel.MouseEnter += new System.EventHandler(this.SettingsLabel_MouseEnter);
            this.SettingsLabel.MouseLeave += new System.EventHandler(this.SettingsLabel_MouseLeave);
            // 
            // LogoutLabel
            // 
            this.LogoutLabel.AutoSize = true;
            this.LogoutLabel.BackColor = System.Drawing.Color.Transparent;
            this.LogoutLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LogoutLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogoutLabel.Location = new System.Drawing.Point(2, 60);
            this.LogoutLabel.Name = "LogoutLabel";
            this.LogoutLabel.Size = new System.Drawing.Size(54, 18);
            this.LogoutLabel.TabIndex = 2;
            this.LogoutLabel.Text = "Logout";
            this.LogoutLabel.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LogoutLabel_MouseClick);
            this.LogoutLabel.MouseEnter += new System.EventHandler(this.LogoutLabel_MouseEnter);
            this.LogoutLabel.MouseLeave += new System.EventHandler(this.LogoutLabel_MouseLeave);
            // 
            // AccountDropDown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.Controls.Add(this.LogoutLabel);
            this.Controls.Add(this.SettingsLabel);
            this.Controls.Add(this.MyAccountLabel);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.Name = "AccountDropDown";
            this.Size = new System.Drawing.Size(90, 90);
            this.Load += new System.EventHandler(this.AccountDropDown_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MyAccountLabel;
        private System.Windows.Forms.Label SettingsLabel;
        private System.Windows.Forms.Label LogoutLabel;
    }
}
