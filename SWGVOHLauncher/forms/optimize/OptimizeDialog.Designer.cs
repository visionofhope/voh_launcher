﻿namespace SWGVOHLauncher.forms.optimize
{
    partial class OptimizeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptimizeDialog));
            this.OptimizeDescription = new System.Windows.Forms.Label();
            this.FileListBox = new System.Windows.Forms.CheckedListBox();
            this.OptimizeButton = new System.Windows.Forms.Button();
            this.AllCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // OptimizeDescription
            // 
            this.OptimizeDescription.AutoSize = true;
            this.OptimizeDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptimizeDescription.ForeColor = System.Drawing.Color.LightGray;
            this.OptimizeDescription.Location = new System.Drawing.Point(12, 9);
            this.OptimizeDescription.Name = "OptimizeDescription";
            this.OptimizeDescription.Size = new System.Drawing.Size(446, 48);
            this.OptimizeDescription.TabIndex = 0;
            this.OptimizeDescription.Text = "below are the files in the game directory that are not\r\nneeded. you may select al" +
    "l, or specific files to purge.";
            this.OptimizeDescription.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FileListBox
            // 
            this.FileListBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.FileListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileListBox.ForeColor = System.Drawing.Color.LightGray;
            this.FileListBox.FormattingEnabled = true;
            this.FileListBox.HorizontalScrollbar = true;
            this.FileListBox.Location = new System.Drawing.Point(12, 84);
            this.FileListBox.Name = "FileListBox";
            this.FileListBox.Size = new System.Drawing.Size(360, 220);
            this.FileListBox.TabIndex = 1;
            // 
            // OptimizeButton
            // 
            this.OptimizeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptimizeButton.Location = new System.Drawing.Point(142, 310);
            this.OptimizeButton.Name = "OptimizeButton";
            this.OptimizeButton.Size = new System.Drawing.Size(100, 35);
            this.OptimizeButton.TabIndex = 2;
            this.OptimizeButton.Text = "optimize";
            this.OptimizeButton.UseVisualStyleBackColor = true;
            this.OptimizeButton.Click += new System.EventHandler(this.OptimizeButton_Click);
            // 
            // AllCheckBox
            // 
            this.AllCheckBox.AutoSize = true;
            this.AllCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllCheckBox.ForeColor = System.Drawing.Color.LightGray;
            this.AllCheckBox.Location = new System.Drawing.Point(15, 60);
            this.AllCheckBox.Name = "AllCheckBox";
            this.AllCheckBox.Size = new System.Drawing.Size(210, 28);
            this.AllCheckBox.TabIndex = 3;
            this.AllCheckBox.Text = "purge all files / folders";
            this.AllCheckBox.UseVisualStyleBackColor = true;
            this.AllCheckBox.CheckStateChanged += new System.EventHandler(this.AllCheckBox_CheckStateChanged);
            // 
            // OptimizeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(384, 361);
            this.Controls.Add(this.FileListBox);
            this.Controls.Add(this.AllCheckBox);
            this.Controls.Add(this.OptimizeButton);
            this.Controls.Add(this.OptimizeDescription);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptimizeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Optimize Dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label OptimizeDescription;
        private System.Windows.Forms.CheckedListBox FileListBox;
        private System.Windows.Forms.Button OptimizeButton;
        private System.Windows.Forms.CheckBox AllCheckBox;
    }
}