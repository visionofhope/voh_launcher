﻿/*************************************************************************
 * Copyright (C) 2016 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using SWGVOHLauncher.src.custom;
using SWGVOHLauncher.src.services.config;
using SWGVOHLauncher.src.services.font;
using SWGVOHLauncher.src.services.movement;
using SWGVOHLauncher.src.services.Reader;
using SWGVOHLauncher.src.services.sound;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SWGVOHLauncher.forms.login_form
{
    public partial class AccountLogin : UserControl
    {
        private LauncherMain Main;

        private Keys[] KeysToIgnore = {
            Keys.ShiftKey,
            Keys.Enter,
            Keys.Tab,
            Keys.LWin,
            Keys.RWin,
            Keys.CapsLock,
            Keys.ControlKey,
            Keys.Up,
            Keys.Down,
            Keys.Left,
            Keys.Right
        };

        public AccountLogin(LauncherMain Main)
        {
            InitializeComponent();
            this.Main = Main;
            BackgroundImage = Properties.Resources.bg_login;
            usernameLabel.Font = CustomFontService.CustomFontFont(usernameLabel);
            passwordLabel.Font = CustomFontService.CustomFontFont(passwordLabel);
            loginButton.Font = CustomFontService.CustomFontFont(loginButton);
            usernameTextBox.Font = CustomFontService.CustomFontFont(usernameTextBox);
            passwordTextBox.Font = CustomFontService.CustomFontFont(passwordTextBox);
            RegisterButton.Font = CustomFontService.CustomFontFont(RegisterButton);
            LoginTitleLabel.Font = CustomFontService.CustomFontFont(LoginTitleLabel);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void AccountLogin_Load(object sender, EventArgs e)
        {
            Location = new Point((Parent.Width - Width) / 2, (Parent.Height - Height) / 2);
            BringToFront();
            Main.BringButtonsToFront(this);
            CustomLabel CustomLabel = new CustomLabel();
            CustomLabel.Text = "SWG VISION OF HOPE © & ™. ALL RIGHTS RESERVED.\nSWG VOH and it's logo are trademarks of SWG Vision of Hope.\nAll other trademarks are the property of their respective owners.";
            CustomLabel.Location = new Point(590, 550);
            Controls.Add(CustomLabel);

            if (!string.IsNullOrEmpty(Properties.Settings.Default.RememberMeUsername) && !string.IsNullOrEmpty(Properties.Settings.Default.RememberMePassword))
            {
                usernameTextBox.Text = Properties.Settings.Default.RememberMeUsername;
                passwordTextBox.Text = Properties.Settings.Default.RememberMePassword;
                RememberMeCheckBox.Checked = true;
                Login();
            }
        }

        private void Login()
        {
            if (string.IsNullOrEmpty(Properties.Settings.Default.RememberMeUsername) || string.IsNullOrEmpty(Properties.Settings.Default.RememberMePassword))
                SoundService.PlaySound("Sound_Click", Main.GetSidePanel());

            if (!AccountLoginIsCorrect())
                return;

            if (RememberMeCheckBox.Checked)
            {
                Properties.Settings.Default.RememberMeUsername = usernameTextBox.Text;
                Properties.Settings.Default.RememberMePassword = passwordTextBox.Text;
                Properties.Settings.Default.Save();
            }

            Program.SetUsername(usernameTextBox.Text);
            Program.SetPassword(passwordTextBox.Text);
            Main.UserLoggedIn();
            Dispose();
        }

        private bool AccountLoginIsCorrect()
        {
            if (string.IsNullOrWhiteSpace(usernameTextBox.Text) || string.IsNullOrWhiteSpace(passwordTextBox.Text))
                return false;

            string auth = Program.WEBSITE_URL + "forums/voh_launcher_auth.php?un=" + usernameTextBox.Text.ToLower() + "&pw=" + passwordTextBox.Text;
            string result = WebClientReader.GetWebPageContent(auth);

            switch (result)
            {
                case "1":
                    return true;
                case "2":
                    MessageBox.Show("You are currently banned.", "You are Banned");
                    return false;
                case "3":
                    MessageBox.Show("Your account is unregistered. Please contact a CSR to verify your account.", "Account Unregistered");
                    return false;
                case "7":
                    WriteCfgs.EnableStaff();
                    return true;
                default:
                    MessageBox.Show("Invalid forum username or password.", "Invalid Username or Password");
                    return false;
            }
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Type(e);

            if (e.KeyCode == Keys.Enter)
                Login();
        }

        private void AccountLogin_MouseDown(object sender, MouseEventArgs e)
        {
            MovementService.SetMouseDownPoint(e);
        }

        private void AccountLogin_MouseMove(object sender, MouseEventArgs e)
        {
            MovementService.MouseMove(this, e);
        }

        private void usernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            Type(e);
        }

        private void Type(KeyEventArgs e)
        {
            foreach (Keys Key in KeysToIgnore)
                if (Key == e.KeyCode)
                    return;
            SoundService.PlaySound("Sound_" + (e.KeyCode != Keys.Back ? "Type" : "Back"), Main.GetSidePanel());
        }

        private void RegisterNowTitle_MouseClick(object sender, MouseEventArgs e)
        {
            SoundService.PlaySound("Sound_Click", Main.GetSidePanel());
            Process.Start(Program.WEBSITE_URL + "forums/register.php");
        }

        private void loginButton_MouseEnter(object sender, EventArgs e)
        {
            Main.GetSidePanel().MouseEnter(loginButton);
        }

        private void loginButton_MouseLeave(object sender, EventArgs e)
        {
            Main.GetSidePanel().MouseLeave(loginButton);
        }

        private void RegisterButton_MouseEnter(object sender, EventArgs e)
        {
            Main.GetSidePanel().MouseEnter(RegisterButton);
        }

        private void RegisterButton_MouseLeave(object sender, EventArgs e)
        {
            Main.GetSidePanel().MouseLeave(RegisterButton);
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            SoundService.PlaySound("Sound_Click", Main.GetSidePanel());
            Process.Start(Program.WEBSITE_URL + "forums/vohregister.php");
        }
    }
}
