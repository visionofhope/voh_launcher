﻿using System.IO;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.config
{
    public class ConfigManager
    {
        public static string[] LauncherConfigOptions = {
            "enableILMAnimations",
            "enableILMGraphics",
            "enableILMMusic",
            "enableILMPlanetMaps",
            "enableILMSoundEffects",
            "enableILMUserInterface",
            "disableCutScenes",
            "runClientAsAdmin",
            "skipIntroScreens",
            "keepLauncherOpen",
            "enableSFX",
            "gameDirectory"
        };

        public static bool SFXIsEnabled()
        {
            return ConfigValueIsTrue("enableSFX");
        }

        public static bool KeepLauncherOpen()
        {
            return ConfigValueIsTrue("keepLauncherOpen");
        }

        public static bool RunClientAsAdmin()
        {
            return ConfigValueIsTrue("runClientAsAdmin");
        }

        public static bool DisableCutScenes()
        {
            return ConfigValueIsTrue("disableCutScenes");
        }

        public static bool SkipSplash()
        {
            return ConfigValueIsTrue("skipIntroScreens");
        }

        public static void MakeLauncherConfigIfDoesntExist()
        {
            if (File.Exists(Application.StartupPath + @"\launcher.cfg"))
                return;

            using (StreamWriter Writer = new StreamWriter(Application.StartupPath + @"\launcher.cfg"))
            {
                for (int i = 0; i < LauncherConfigOptions.Length - 1; i++)
                    Writer.WriteLine(LauncherConfigOptions[i] + "=0");
                Writer.WriteLine(LauncherConfigOptions[LauncherConfigOptions.Length - 1] + "=");
                Writer.Close();
            }
        }

        public static bool ConfigValueIsTrue(string Key)
        {
            string Line;
            using (StreamReader Reader = new StreamReader(Application.StartupPath + @"\launcher.cfg"))
            {
                while ((Line = Reader.ReadLine()) != null)
                {
                    if (Line.StartsWith(Key))
                        break;
                }
                Reader.Close();
            }

            if (!string.IsNullOrEmpty(Line))
                return Line.Replace(Key + "=", "").Equals("1");
            return false;

        }

        public static bool ConfigValueIsTrue(int i)
        {
            string Line;
            using (StreamReader Reader = new StreamReader(Application.StartupPath + @"\launcher.cfg"))
            {
                
                while ((Line = Reader.ReadLine()) != null)
                {
                    if (Line.StartsWith(LauncherConfigOptions[i]))
                        break;
                }
                Reader.Close();
            }
            if (!string.IsNullOrEmpty(Line))
                return Line.Replace(LauncherConfigOptions[i] + "=", "").Equals("1");
            return false;
        }

        public static string GetGameDirectory()
        {
            string Line;
            using (StreamReader Reader = new StreamReader(Application.StartupPath + @"\launcher.cfg"))
            {
                while ((Line = Reader.ReadLine()) != null)
                {
                    if (Line.StartsWith("gameDirectory="))
                        break;
                }
                Reader.Close();
            }
            if (!string.IsNullOrEmpty(Line))
                return Line.Replace("gameDirectory=", "");
            return null;
        }

        public static void ChangeConfig(string Key, string NewValue)
        {
            string[] Lines;

            using (StreamReader Reader = new StreamReader(Application.StartupPath + @"\launcher.cfg"))
            {
                Lines = Reader.ReadToEnd().Replace("\r", string.Empty).Split('\n');
                Reader.Close();
            }

            string[] Keys = new string[Lines.Length - 1];
            string[] Values = new string[Lines.Length - 1];

            for (int i = 0; i < Keys.Length; i++)
            {
                string[] SplitLine = Lines[i].Split('=');
                Keys[i] = SplitLine[0];
                Values[i] = SplitLine[1];
            }

            using (StreamWriter Writer = new StreamWriter(Application.StartupPath + @"\launcher.cfg"))
            {
                for (int i = 0; i < Keys.Length; i++)
                {
                    if (Keys[i].Equals(Key))
                        Writer.WriteLine(Key + "=" + NewValue);
                    else
                        Writer.WriteLine(Keys[i] + "=" + Values[i]);
                }
                Writer.Close();
            }
        }

        public static void ChangeConfig(string[] Values)
        {
            using (StreamWriter Writer = new StreamWriter(Application.StartupPath + @"\launcher.cfg"))
            {
                for (int i = 0; i < Values.Length; i++)
                {
                    Writer.WriteLine(LauncherConfigOptions[i] + "=" + Values[i]);
                }
                Writer.Close();
            }
        }
    }
}
