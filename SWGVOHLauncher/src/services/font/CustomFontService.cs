﻿using System;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.font
{
    class CustomFontService
    {
        [DllImport("gdi32.dll")]
        private static extern IntPtr AddFontMemResourceEx(IntPtr pbFont, uint cbFont, IntPtr pdv, [In] ref uint pcFonts);

        private static readonly PrivateFontCollection CustomFont = new PrivateFontCollection();

        public static void LoadCustomFont()
        {
            uint dummy = 0;
            byte[] fontData = Properties.Resources.customfont;
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            
            fontData = Properties.Resources.customfont;
            fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            CustomFont.AddMemoryFont(fontPtr, Properties.Resources.customfont.Length);
            AddFontMemResourceEx(fontPtr, (uint)Properties.Resources.customfont.Length, IntPtr.Zero, ref dummy);
            Marshal.FreeCoTaskMem(fontPtr);
        }

        public static Font CustomFontFont(Control control)
        {
            return new Font(CustomFont.Families[0], control.Font.Size);
        }

        public static Font CustomFontFont(int Size)
        {
            return new Font(CustomFont.Families[0], Size);
        }
    }
}
