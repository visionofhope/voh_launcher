﻿using System.Net;

namespace SWGVOHLauncher.src.services.Reader
{
    public class WebClientReader
    {
        public static string GetWebPageContent(string Url)
        {
            using (WebClient WebClient = new WebClient())
            {
                WebClient.Proxy = GlobalProxySelection.GetEmptyWebProxy();
                return WebClient.DownloadString(Url);
            }
        }
    }
}
