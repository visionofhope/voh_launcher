﻿using System.Drawing;
using System.Windows.Forms;

namespace SWGVOHLauncher.src.services.movement
{
    class MovementService
    {
        private static Point p;

        public static void MouseMove(Control control, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (control is LauncherMain)
                    control.Location = new Point(control.Location.X + (e.X - p.X), control.Location.Y + (e.Y - p.Y));
                else
                    control.Parent.Location = new Point(control.Parent.Location.X + (e.X - p.X), control.Parent.Location.Y + (e.Y - p.Y));
            }
        }

        public static void SetMouseDownPoint(MouseEventArgs e)
        {
            p = e.Location;
        }
    }
}
