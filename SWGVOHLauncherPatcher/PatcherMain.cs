﻿/*************************************************************************
 * Copyright (C) 2016 <SWG Vision of Hope>
 *
 * This file is part of the SWG Vision of Hope Launcher.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *************************************************************************/
// @author: Iosnowore
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows.Forms;

namespace SWGVOHLauncherPatcher
{
    public partial class PatcherMain : Form
    {
        public PatcherMain()
        {
            InitializeComponent();
        }

        private void PatcherMain_Load(object sender, EventArgs e)
        {
            DownloadPatch(Program.LAUNCHER);
        }

        private void DownloadPatch(string Patch)
        {
            using (WebClient client = new WebClient())
            {
                client.Proxy = null;
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileCompleted += Client_DownloadFileCompleted;
                client.DownloadFileAsync(new Uri(Program.PATCH_URL + Patch), Application.StartupPath + @"\" + Patch);
                client.Dispose();
            }
        }

        private void Client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Process.Start(Application.StartupPath + @"\" + Program.LAUNCHER);
            Dispose();

        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            PatchingProgressBar.Maximum = (int)e.TotalBytesToReceive / 100;
            PatchingProgressBar.Value = (int)e.BytesReceived / 100;
            PercentFinished.Text = (int)(PatchingProgressBar.Value / (float)(PatchingProgressBar.Maximum) * 100) + "% Finished";
        }

        private void PatcherMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PatchingProgressBar.Maximum > PatchingProgressBar.Value && PatchingProgressBar.Value != 0)
            {
                DialogResult dr = MessageBox.Show("The SWG Vision of Hope launcher is updating are you sure you want to exit? It may cause damage.", "Exit?", MessageBoxButtons.YesNo);

                if (dr == DialogResult.No)
                    e.Cancel = true;
            }
        }
    }
}
