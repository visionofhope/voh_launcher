﻿namespace PatchListCreator
{
    partial class PatchListCreator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PatchDirectory = new System.Windows.Forms.TextBox();
            this.CreateButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PatchDirectory
            // 
            this.PatchDirectory.Location = new System.Drawing.Point(12, 12);
            this.PatchDirectory.Name = "PatchDirectory";
            this.PatchDirectory.Size = new System.Drawing.Size(260, 20);
            this.PatchDirectory.TabIndex = 0;
            this.PatchDirectory.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PatchDirectory_MouseClick);
            // 
            // CreateButton
            // 
            this.CreateButton.Location = new System.Drawing.Point(92, 38);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(75, 23);
            this.CreateButton.TabIndex = 1;
            this.CreateButton.Text = "Create";
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // PatchListCreator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.PatchDirectory);
            this.Name = "PatchListCreator";
            this.Text = "Patch List Creator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PatchDirectory;
        private System.Windows.Forms.Button CreateButton;
    }
}

