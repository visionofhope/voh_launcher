﻿using ResourceManager.src.services.font;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SWGVOHGalaxyHarvester
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            new Form1(this).ShowDialog();

            Control[] Controls = {
                CloseButton,
                FilterLabel,
                ResourceClassLabel,
                ClassListBox,
                ResetButton,
                ResourceManagerTitleLabel,
                FiltersButton,
                TextBox,
                SearchBox,
                SearchLabel
            };

            for (int i = 0; i < Controls.Length; i++)
               Controls[i].Font = CustomFontService.CustomFontFont(Controls[i]);
        }

        public void SetTextBoxText(string Text)
        {
            TextBox.Text = Text;
        }

        public void AddRangeToClassListBox()
        {
            ClassListBox.Items.AddRange(Resource.ClassStringsFormatted);
        }

        private void ClassComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            string TextBoxText = string.Empty;

            for (int i = 0; i < Program.GetResourcesLength(); i++)
            {
                if (Program.GetResource(i).GetFormattedClass().Equals(ClassListBox.SelectedItem))
                    TextBoxText += Program.GetResource(i).ToString();
            }
            TextBox.Text = TextBoxText;
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            string NewText = string.Empty;
            ClassListBox.SelectedIndex = -1;
            for (int i = 0; i < Program.GetResourcesLength(); i++)
                NewText += Program.GetResource(i).ToString();
            TextBox.Text = NewText;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ResourceClassLabel.Location = new Point((FilterPanel.Width - ResourceClassLabel.Width) / 2, ResourceClassLabel.Location.Y);
            FilterLabel.Location = new Point((FilterPanel.Width - FilterLabel.Width) / 2, FilterLabel.Location.Y);
            SearchBox.Location = new Point((FilterPanel.Width - SearchBox.Width) / 2, SearchBox.Location.Y);
        }

        private void FiltersButton_Click(object sender, EventArgs e)
        {
            FilterPanel.Visible = true; ;
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            FilterPanel.Visible = false;
        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            string NewText = string.Empty;

            for (int i = 0; i < Program.GetResourcesLength(); i++)
            {
                if (Program.GetResource(i).HasKeyWord(SearchBox.Text))
                    NewText += Program.GetResource(i).ToString();
            }
            TextBox.Text = NewText;
        }
    }
}
