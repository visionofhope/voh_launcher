﻿namespace SWGVOHGalaxyHarvester
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.TextBox = new System.Windows.Forms.RichTextBox();
            this.ResourceManagerTitleLabel = new System.Windows.Forms.Label();
            this.FiltersButton = new System.Windows.Forms.Button();
            this.FilterPanel = new System.Windows.Forms.Panel();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.ResetButton = new System.Windows.Forms.Button();
            this.CloseButton = new System.Windows.Forms.Button();
            this.FilterLabel = new System.Windows.Forms.Label();
            this.ResourceClassLabel = new System.Windows.Forms.Label();
            this.ClassListBox = new System.Windows.Forms.ListBox();
            this.FilterPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox.ForeColor = System.Drawing.Color.LightGray;
            this.TextBox.Location = new System.Drawing.Point(12, 12);
            this.TextBox.Name = "TextBox";
            this.TextBox.ReadOnly = true;
            this.TextBox.Size = new System.Drawing.Size(425, 567);
            this.TextBox.TabIndex = 0;
            this.TextBox.Text = "";
            // 
            // ResourceManagerTitleLabel
            // 
            this.ResourceManagerTitleLabel.AutoSize = true;
            this.ResourceManagerTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.ResourceManagerTitleLabel.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResourceManagerTitleLabel.ForeColor = System.Drawing.Color.White;
            this.ResourceManagerTitleLabel.Location = new System.Drawing.Point(532, 498);
            this.ResourceManagerTitleLabel.Name = "ResourceManagerTitleLabel";
            this.ResourceManagerTitleLabel.Size = new System.Drawing.Size(319, 84);
            this.ResourceManagerTitleLabel.TabIndex = 1;
            this.ResourceManagerTitleLabel.Text = "swg vision of hope\r\nresource manager";
            // 
            // FiltersButton
            // 
            this.FiltersButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FiltersButton.Location = new System.Drawing.Point(443, 12);
            this.FiltersButton.Name = "FiltersButton";
            this.FiltersButton.Size = new System.Drawing.Size(90, 30);
            this.FiltersButton.TabIndex = 6;
            this.FiltersButton.Text = "Filters";
            this.FiltersButton.UseVisualStyleBackColor = true;
            this.FiltersButton.Click += new System.EventHandler(this.FiltersButton_Click);
            // 
            // FilterPanel
            // 
            this.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.FilterPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FilterPanel.Controls.Add(this.ClassListBox);
            this.FilterPanel.Controls.Add(this.SearchLabel);
            this.FilterPanel.Controls.Add(this.SearchBox);
            this.FilterPanel.Controls.Add(this.ResetButton);
            this.FilterPanel.Controls.Add(this.CloseButton);
            this.FilterPanel.Controls.Add(this.FilterLabel);
            this.FilterPanel.Controls.Add(this.ResourceClassLabel);
            this.FilterPanel.Location = new System.Drawing.Point(443, 48);
            this.FilterPanel.Name = "FilterPanel";
            this.FilterPanel.Size = new System.Drawing.Size(459, 447);
            this.FilterPanel.TabIndex = 7;
            this.FilterPanel.Visible = false;
            // 
            // SearchLabel
            // 
            this.SearchLabel.AutoSize = true;
            this.SearchLabel.BackColor = System.Drawing.Color.Transparent;
            this.SearchLabel.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchLabel.ForeColor = System.Drawing.Color.LightGray;
            this.SearchLabel.Location = new System.Drawing.Point(102, 73);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(101, 32);
            this.SearchLabel.TabIndex = 11;
            this.SearchLabel.Text = "Search";
            // 
            // SearchBox
            // 
            this.SearchBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchBox.Location = new System.Drawing.Point(77, 108);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(300, 31);
            this.SearchBox.TabIndex = 10;
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            // 
            // ResetButton
            // 
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResetButton.Location = new System.Drawing.Point(235, 410);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(90, 30);
            this.ResetButton.TabIndex = 9;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // CloseButton
            // 
            this.CloseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseButton.Location = new System.Drawing.Point(130, 410);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(95, 30);
            this.CloseButton.TabIndex = 8;
            this.CloseButton.Text = "Close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            // 
            // FilterLabel
            // 
            this.FilterLabel.AutoSize = true;
            this.FilterLabel.BackColor = System.Drawing.Color.Transparent;
            this.FilterLabel.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FilterLabel.ForeColor = System.Drawing.Color.LightGray;
            this.FilterLabel.Location = new System.Drawing.Point(171, 11);
            this.FilterLabel.Name = "FilterLabel";
            this.FilterLabel.Size = new System.Drawing.Size(102, 36);
            this.FilterLabel.TabIndex = 7;
            this.FilterLabel.Text = "Filters";
            // 
            // ResourceClassLabel
            // 
            this.ResourceClassLabel.AutoSize = true;
            this.ResourceClassLabel.BackColor = System.Drawing.Color.Transparent;
            this.ResourceClassLabel.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ResourceClassLabel.ForeColor = System.Drawing.Color.LightGray;
            this.ResourceClassLabel.Location = new System.Drawing.Point(116, 170);
            this.ResourceClassLabel.Name = "ResourceClassLabel";
            this.ResourceClassLabel.Size = new System.Drawing.Size(209, 32);
            this.ResourceClassLabel.TabIndex = 6;
            this.ResourceClassLabel.Text = "Resource Class";
            // 
            // ClassListBox
            // 
            this.ClassListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ClassListBox.FormattingEnabled = true;
            this.ClassListBox.ItemHeight = 25;
            this.ClassListBox.Location = new System.Drawing.Point(77, 205);
            this.ClassListBox.Name = "ClassListBox";
            this.ClassListBox.Size = new System.Drawing.Size(300, 129);
            this.ClassListBox.TabIndex = 12;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::ResourceManager.Properties.Resources.bg4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(914, 591);
            this.Controls.Add(this.FilterPanel);
            this.Controls.Add(this.FiltersButton);
            this.Controls.Add(this.ResourceManagerTitleLabel);
            this.Controls.Add(this.TextBox);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "[SWG Vision of Hope] Resource Manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FilterPanel.ResumeLayout(false);
            this.FilterPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox TextBox;
        private System.Windows.Forms.Label ResourceManagerTitleLabel;
        private System.Windows.Forms.Button FiltersButton;
        private System.Windows.Forms.Panel FilterPanel;
        private System.Windows.Forms.Label FilterLabel;
        private System.Windows.Forms.Label ResourceClassLabel;
        private System.Windows.Forms.Button CloseButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Label SearchLabel;
        private System.Windows.Forms.ListBox ClassListBox;
    }
}