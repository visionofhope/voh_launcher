﻿using ResourceManager.src.services.font;
using System;
using System.Windows.Forms;

namespace SWGVOHGalaxyHarvester
{
    public static class Program
    {
        private static Resource[] Resources;

        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            CustomFontService.LoadCustomFont();
            Application.Run(new MainForm());
        }

        public static void SetResourcesLength(int length)
        {
            Resources = new Resource[length];
        }

        public static int GetResourcesLength()
        {
            return Resources.Length;
        }

        public static Resource GetResource(int index)
        {
            return Resources[index];
        }

        public static void SetResource(string resource, int index)
        {
            Resources[index] = new Resource(resource);
        }
    }
}
