﻿namespace SWGVOHMacroEditor
{
    public class Macro
    {
        private string number;
        private string name;
        private string icon;
        private string contents;

        public Macro(string line)
        {
            string[] macroStuff = line.Split(' ');
            number = macroStuff[0];
            name = macroStuff[1];
            icon = macroStuff[2];
            contents = line.Substring(line.IndexOf("#ffffff"));
        }

        public void SetContents(string contents)
        {
            this.contents = contents;
        }

        public string GetContents()
        {
            return contents;
        }

        public string GetName()
        {
            return name;
        }

        public new string ToString()
        {
            return number + " " + name + " " + icon + " " + contents;
        }
    }
}
