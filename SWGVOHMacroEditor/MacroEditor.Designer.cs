﻿namespace SWGVOHMacroEditor
{
    partial class MacroEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MacroEditor));
            this.ProfileUserComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.MacroTextBox = new System.Windows.Forms.RichTextBox();
            this.SaveButton = new System.Windows.Forms.Button();
            this.RevertButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.MacroNameComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ProfileUserComboBox
            // 
            this.ProfileUserComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ProfileUserComboBox.FormattingEnabled = true;
            this.ProfileUserComboBox.Location = new System.Drawing.Point(12, 28);
            this.ProfileUserComboBox.Name = "ProfileUserComboBox";
            this.ProfileUserComboBox.Size = new System.Drawing.Size(121, 21);
            this.ProfileUserComboBox.TabIndex = 0;
            this.ProfileUserComboBox.SelectedIndexChanged += new System.EventHandler(this.ProfileUserComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Profile Username";
            // 
            // MacroTextBox
            // 
            this.MacroTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MacroTextBox.Location = new System.Drawing.Point(139, 12);
            this.MacroTextBox.Name = "MacroTextBox";
            this.MacroTextBox.Size = new System.Drawing.Size(358, 208);
            this.MacroTextBox.TabIndex = 2;
            this.MacroTextBox.Text = "";
            this.MacroTextBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MacroTextBox_MouseDown);
            // 
            // SaveButton
            // 
            this.SaveButton.Enabled = false;
            this.SaveButton.Location = new System.Drawing.Point(422, 226);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(75, 23);
            this.SaveButton.TabIndex = 3;
            this.SaveButton.Text = "Save";
            this.SaveButton.UseVisualStyleBackColor = true;
            // 
            // RevertButton
            // 
            this.RevertButton.Enabled = false;
            this.RevertButton.Location = new System.Drawing.Point(341, 226);
            this.RevertButton.Name = "RevertButton";
            this.RevertButton.Size = new System.Drawing.Size(75, 23);
            this.RevertButton.TabIndex = 4;
            this.RevertButton.Text = "Revert";
            this.RevertButton.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Macro Name";
            // 
            // MacroNameComboBox
            // 
            this.MacroNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MacroNameComboBox.Enabled = false;
            this.MacroNameComboBox.FormattingEnabled = true;
            this.MacroNameComboBox.Location = new System.Drawing.Point(12, 79);
            this.MacroNameComboBox.Name = "MacroNameComboBox";
            this.MacroNameComboBox.Size = new System.Drawing.Size(121, 21);
            this.MacroNameComboBox.TabIndex = 5;
            this.MacroNameComboBox.SelectedIndexChanged += new System.EventHandler(this.MacroNameComboBox_SelectedIndexChanged);
            // 
            // MacroEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(37)))), ((int)(((byte)(36)))));
            this.ClientSize = new System.Drawing.Size(509, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.MacroNameComboBox);
            this.Controls.Add(this.RevertButton);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.MacroTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ProfileUserComboBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MacroEditor";
            this.Text = "Macro Editor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ProfileUserComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox MacroTextBox;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.Button RevertButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox MacroNameComboBox;
    }
}